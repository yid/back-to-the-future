﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using GameLogic;
using Constants;
using Conditions;
using Android.Views.Animations;
using Android.Content.Res;
using Android.Graphics;
using Android.Graphics.Drawables;

namespace Back_to_the_future
{
    [Activity(ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@android:style/Theme.Black.NoTitleBar")]
    public class Game : Activity
    {
        private TheGame game;
        private ScrollView svForGame;
        private ImageView imageViewPicture;
        private TextView textViewDisription;
        private ImageView imageViewCar;
        private LinearLayout linearLayout;
        private string[] Descriptions;
        private string[] Links;
        private string[] YearConditions;
        private TypedArray NodePictures;
        private List<Button> buttons;
        private List<EventHandler> buttonEvents;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Game);

            svForGame = (ScrollView)FindViewById(Resource.Id.scrollViewForGame);
            imageViewPicture = (ImageView)FindViewById(Resource.Id.imageViewPicture);
            textViewDisription = (TextView)FindViewById(Resource.Id.textViewDescription);
            imageViewCar = (ImageView)FindViewById(Resource.Id.imageViewCar);
            linearLayout = (LinearLayout)FindViewById(Resource.Id.linearLayoutForButtons);
            Descriptions = Resources.GetStringArray(Resource.Array.Descriptions);
            Links = Resources.GetStringArray(Resource.Array.Links);
            YearConditions = Resources.GetStringArray(Resource.Array.YearConditions);
            NodePictures = Resources.ObtainTypedArray(Resource.Array.NodePictures);
            buttons = new List<Button>();
            buttonEvents = new List<EventHandler>();

            switch (Intent.GetIntExtra(Const.TypeOfGame, 1))
            {
                case 1:
                    ISharedPreferences pref = GetPreferences(FileCreationMode.Private);
                    game = new TheGame(this, pref.GetInt(Const.SavedGame, Const.NumberOfStartNode));
                    break;
                case 0:
                    game = new TheGame(this);
                    Intent intent = new Intent(this, typeof(Info));
                    intent.PutExtra(Const.TypeOfInfo, 2);
                    StartActivity(intent);
                    break;
            }
        }
        protected override void OnDestroy()
        {
            base.OnDestroy();
            game = null;
            svForGame.RemoveAllViews();
            svForGame.Dispose();
            svForGame = null;
            linearLayout.RemoveAllViews();
            linearLayout.Dispose();
            linearLayout = null;
            imageViewPicture.Drawable.Dispose();
            imageViewPicture.SetImageDrawable(null);
            imageViewPicture.Dispose();
            imageViewPicture = null;
            textViewDisription.Dispose();
            textViewDisription = null;
            imageViewCar.Drawable.Dispose();
            imageViewCar.SetImageDrawable(null);
            imageViewCar.Dispose();
            imageViewCar = null;
            NodePictures.Dispose();
            NodePictures = null;
            for (int i = 0; i < buttons.Count; i++)
            {
                buttons[i].Background.Dispose();
                buttons[i].SetBackgroundDrawable(null);
                buttons[i].Click -= buttonEvents[i];
                buttonEvents[i] = null;
                buttons[i].Dispose();
                buttons[i] = null;
            }
        }
        private void Show()
        {
            imageViewCar.Visibility = Android.Views.ViewStates.Visible;
            for (int i = 0; i < buttons.Count; i++)
            {
                linearLayout.RemoveView(buttons[i]);
                buttons[i].Background.Dispose();
                buttons[i].SetBackgroundDrawable(null);
                buttons[i].Click -= buttonEvents[i];
                buttonEvents[i] = null;
                buttons[i].Dispose();
                buttons[i] = null;
            }
            buttons.Clear();
            buttonEvents.Clear();
            imageViewPicture.Drawable.Dispose();

            List<int> temp = game.GetCurrentPositionInformation();
            imageViewPicture.SetImageDrawable(NodePictures.GetDrawable(temp[0]));
            textViewDisription.SetText(Descriptions[temp[0]], TextView.BufferType.Normal);
            for (int i = 2; i < temp.Count; i++)
            {
                Button button = new Button(this);
                if (temp[1] == 1)
                    button.SetText(Descriptions[temp[i]], TextView.BufferType.Normal);
                else
                    button.SetText(Links[temp[i]], TextView.BufferType.Normal);
                buttonEvents.Add(FunctionFactory(temp[i]));
                button.Click += buttonEvents[buttonEvents.Count - 1];
                button.SetBackgroundResource(Resource.Drawable.button_background);
                button.SetTextSize(Android.Util.ComplexUnitType.Dip, 20);
                button.SetTextColor(Color.White);
                buttons.Add(button);
                linearLayout.AddView(button);
            }
        }
        private EventHandler FunctionFactory(int arg)
        {
            return (object IntentSender, EventArgs e) =>
            {
                Intent intent = new Intent(this, typeof(Cartoon));
                RunOnUiThread(new Action(() =>
                    {
                        List<int> ChangesList = game.Travel(arg);

                        ISharedPreferences pref = GetPreferences(FileCreationMode.Private);
                        ISharedPreferencesEditor edit = pref.Edit();
                        edit.PutInt(Const.SavedGame, game.PresentPosition);
                        edit.Commit();

                        PictureCondition[] PictureConditionChanges = new PictureCondition[ChangesList.Count];
                        int NumberOfFrames = Const.CartoonLength / PictureConditionChanges.Length;
                        intent.PutExtra(Const.NumberOfFrames, NumberOfFrames);
                        YearCondition tempYearCondition1 = new YearCondition(YearConditions[ChangesList[0]]);
                        YearCondition tempYearCondition2 = null;
                        intent.PutExtra(Const.LastDeparted, (int)tempYearCondition1.Year);
                        PictureConditionChanges[0] = new PictureCondition(tempYearCondition1);
                        for (int i = 1; i < PictureConditionChanges.Length; i++)
                        {
                            tempYearCondition1 = null;
                            tempYearCondition2 = null;
                            tempYearCondition1 = new YearCondition(YearConditions[ChangesList[i - 1]]);
                            tempYearCondition2 = new YearCondition(YearConditions[ChangesList[i]]);
                            PictureConditionChanges[i] = new PictureCondition(new PictureCondition(tempYearCondition1), new PictureCondition(tempYearCondition2), NumberOfFrames);
                        }
                        intent.PutExtra(Const.DestinationTime, (int)tempYearCondition2.Year);
                        intent.PutExtra(Const.PictureConditions, PictureConditionChanges);
                    }));
                Animation anim = AnimationUtils.LoadAnimation(this, Resource.Animation.GoAway);
                anim.AnimationEnd += (object sender, Animation.AnimationEndEventArgs ee) => { imageViewCar.Visibility = Android.Views.ViewStates.Invisible; StartActivityForResult(intent, 0); };
                imageViewCar.StartAnimation(anim);
            };
        }
        protected override void OnResume()
        {
            base.OnResume();
            Show();
            imageViewCar.StartAnimation(AnimationUtils.LoadAnimation(this, Resource.Animation.ComeOn));
        }
        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            if (resultCode != Result.Ok)
                return;

            BitmapDrawable b = (BitmapDrawable)NodePictures.GetDrawable(game.PresentPosition);
            String pathofBmp = Android.Provider.MediaStore.Images.Media.InsertImage(ContentResolver, b.Bitmap, "title", null);

            Android.Net.Uri[] Uris = new Android.Net.Uri[2];
            Uris[0] = Android.Net.Uri.Parse(data.GetStringExtra("Result"));
            Uris[1] = Android.Net.Uri.Parse(pathofBmp);

            Intent share = new Intent(Intent.ActionSendMultiple);
            share.PutExtra(Intent.ExtraText, Descriptions[game.PresentPosition]);
            share.PutParcelableArrayListExtra(Intent.ExtraStream, Uris);
            share.SetType("image/*");
            StartActivity(Intent.CreateChooser(share, "Share this via"));
        }
    }
}

