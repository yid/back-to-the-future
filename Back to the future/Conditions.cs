﻿using Android.OS;
using Java.Interop;

namespace Conditions
{
    /// <summary>
    /// Представляет информацию о картинке
    /// </summary>
    public class PictureCondition : Java.Lang.Object, IParcelable
    {
        /// <summary>
        /// Количество человеческих домов. От 0 до 1
        /// </summary>
        public float NumberOfHumansBuildings { get { return Values[0]; } private set { Values[0] = value; } }
        /// <summary>
        /// Количество инопланетных домов. От 0 до 1
        /// </summary>
        public float NumberOfAliensBuildings { get { return Values[1]; } private set { Values[1] = value; } }
        /// <summary>
        /// Количество домов для роботов. От 0 до 1
        /// </summary>
        public float NumberOfRobotsBuildings { get { return Values[2]; } private set { Values[2] = value; } }
        /// <summary>
        /// Загрязнение. Влияет на количество деревьев и цвет некоторых элементов картины. От 0 до 1. 0 - хорошая экология, 1 - высокий уровень загрязнения
        /// </summary>
        public float Pollution { get { return Values[3]; } private set { Values[3] = value; } }
        /// <summary>
        /// Вероятность появления пешехода. От 0 до 1
        /// </summary>
        public float WalkerProbability { get { return Values[4]; } private set { Values[4] = value; } }
        /// <summary>
        /// Вероятность того, что пешеход - животное. От 0 до 1
        /// </summary>
        public float WalkerIsAnimalProbability { get { return Values[5]; } private set { Values[5] = value; } }
        /// <summary>
        /// Вероятность того, что пешеход - инопланетянин. От 0 до 1
        /// </summary>
        public float WalkerIsAlienProbability { get { return Values[6]; } private set { Values[6] = value; } }
        /// <summary>
        /// Вероятность того, что пешеход - робот. От 0 до 1
        /// </summary>
        public float WalkerIsRobotProbability { get { return Values[7]; } private set { Values[7] = value; } }
        /// <summary>
        /// Размер животного. От 0 до 1
        /// </summary>
        public float AnimalSize { get { return Values[8]; } private set { Values[8] = value; } }
        /// <summary>
        /// Вероятность появления транспорта. От 0 до 1
        /// </summary>
        public float TransportProbability { get { return Values[9]; } private set { Values[9] = value; } }
        /// <summary>
        /// Вероятность того, что транспорт - летающая тарелка. От 0 до 1
        /// </summary>
        public float TransportIsUfoProbability { get { return Values[10]; } private set { Values[10] = value; } }
        /// <summary>
        /// Вероятность появления взрыва. От 0 до 1
        /// </summary>
        public float ExplosionProbability { get { return Values[11]; } private set { Values[11] = value; } }
        /// <summary>
        /// Скорость транспорта. От 0 до 1
        /// </summary>
        public float TransportSpeed { get { return Values[12]; } private set { Values[12] = value; } }
        /// <summary>
        /// Текущий год
        /// </summary>
        public float Year { get { return Values[13]; } private set { Values[13] = value; } }
        /// <summary>
        /// Конструктор, преобразующий информацию о времени в информацию о картинке
        /// </summary>
        /// <param name="yearCondition">Информация о времени</param>
        public PictureCondition(YearCondition yearCondition)
        {
            NumberOfHumansBuildings = yearCondition.NumberOfHumans;
            NumberOfAliensBuildings = yearCondition.NumberOfAliens;
            NumberOfRobotsBuildings = yearCondition.NumberOfRobots;
            Pollution = yearCondition.Pollution;
            WalkerProbability = (yearCondition.NumberOfAliens + yearCondition.NumberOfHumans + yearCondition.NumberOfAnimals + yearCondition.NumberOfRobots) / 4f;
            WalkerIsAnimalProbability = yearCondition.NumberOfAnimals / (yearCondition.NumberOfAliens + yearCondition.NumberOfHumans + yearCondition.NumberOfAnimals + yearCondition.NumberOfRobots);
            WalkerIsAlienProbability = yearCondition.NumberOfAliens / (yearCondition.NumberOfAliens + yearCondition.NumberOfHumans + yearCondition.NumberOfAnimals + yearCondition.NumberOfRobots);
            WalkerIsRobotProbability = yearCondition.NumberOfRobots / (yearCondition.NumberOfAliens + yearCondition.NumberOfHumans + yearCondition.NumberOfAnimals + yearCondition.NumberOfRobots);
            AnimalSize = yearCondition.NumberOfAnimals;
            TransportProbability = (yearCondition.NumberOfAliens + yearCondition.NumberOfHumans) * yearCondition.TransportLevel / 2f;
            TransportIsUfoProbability = yearCondition.NumberOfAliens / (yearCondition.NumberOfAliens + yearCondition.NumberOfHumans);
            TransportSpeed = yearCondition.TransportLevel;
            ExplosionProbability = yearCondition.Violence;
            Year = yearCondition.Year;
        }
        /// <summary>
        /// Конструктор, возвращающий разницу между двумя существующими PictureCondition. Используется для описания изменений между двумя картинками.
        /// </summary>
        /// <param name="PreviousPictureCondition">Начальное состояние</param>
        /// <param name="NextPictureCondition">Конечное состояние</param>
        /// <param name="NumberOfParts">Количество частей, на которое делится отрезок, соединяющий начальное и конечное состояние</param>
        public PictureCondition(PictureCondition PreviousPictureCondition, PictureCondition NextPictureCondition, int NumberOfParts)
        {
            for (int i = 0; i < Values.Length; i++)
                Values[i] = (PreviousPictureCondition.Values[i] - NextPictureCondition.Values[i]) / NumberOfParts;
        }
        /// <summary>
        /// Прибавляет к текущему состоянию другое состояние (полученное при помощи конструктора вычитания состояний)
        /// </summary>
        /// <param name="addition">Прибавляемое состояние (полученное при помощи конструктора вычитания состояний)</param>
        public void Add(PictureCondition addition)
        {
            for (int i = 0; i < Values.Length; i++)
                Values[i] -= addition.Values[i];
        }
        [ExportField("CREATOR")]
        public static PictureConditionCreator InititalizeCreator()
        {
            return new PictureConditionCreator();
        }

        public class PictureConditionCreator : Java.Lang.Object, IParcelableCreator
        {
            public Java.Lang.Object CreateFromParcel(Parcel source)
            {
                return new PictureCondition(source);
            }

            public Java.Lang.Object[] NewArray(int size)
            {
                return new Java.Lang.Object[size];
            }
        }

        public int DescribeContents()
        {
            return 0;
        }

        public void WriteToParcel(Parcel dest, ParcelableWriteFlags flags)
        {
            dest.WriteFloatArray(Values);
        }
        public PictureCondition(Parcel p)
        {
            p.ReadFloatArray(Values);
        }
        private float[] Values = new float[14];
    }
    /// <summary>
    /// Информация о времени
    /// </summary>
    public class YearCondition
    {
        public readonly float MaxValue;
        /// <summary>
        /// Число людей. От 0 до 1
        /// </summary>
        public readonly float NumberOfHumans;
        /// <summary>
        /// Число инопланетян. От 0 до 1
        /// </summary>
        public readonly float NumberOfAliens;
        /// <summary>
        /// Число роботов. От 0 до 1
        /// </summary>
        public readonly float NumberOfRobots;
        /// <summary>
        /// Число животных. От 0 до 1
        /// </summary>
        public readonly float NumberOfAnimals;
        /// <summary>
        /// Загрязнение. От 0 до 1
        /// </summary>
        public readonly float Pollution;
        /// <summary>
        /// Уровень транспорта. От 0 до 1
        /// </summary>
        public readonly float TransportLevel;
        /// <summary>
        /// Агрессивность. От 0 до 1
        /// </summary>
        public readonly float Violence;
        /// <summary>
        /// Текущий год
        /// </summary>
        public readonly float Year;
        /// <summary>
        /// Парсит строку и восстанавливает YearCondition
        /// </summary>
        /// <param name="ConditionString">Строка с содержимым YearCondition (набор из 8 целых цифр, разделенных запятой. Первая из них диапазона int - это год, остальные - byte - параметры в следущем порядке: количество людей, количество инопланетян, уровень технологического развития, количество животных, уровень загрязнения, уровень транспорта, уровень агрессивности</param>
        public YearCondition(string ConditionString)
        {
            string[] temp = ConditionString.Split(',');
            Year = int.Parse(temp[0]);
            NumberOfHumans = (float)byte.Parse(temp[1]) / byte.MaxValue;
            NumberOfAliens = (float)byte.Parse(temp[2]) / byte.MaxValue;
            NumberOfRobots = (float)byte.Parse(temp[3]) / byte.MaxValue;
            NumberOfAnimals = (float)byte.Parse(temp[4]) / byte.MaxValue;
            Pollution = (float)byte.Parse(temp[5]) / byte.MaxValue;
            TransportLevel = (float)byte.Parse(temp[6]) / byte.MaxValue;
            Violence = (float)byte.Parse(temp[7]) / byte.MaxValue;
        }
    }
}