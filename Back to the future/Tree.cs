﻿using System.Collections.Generic;

namespace Tree
{
    /// <summary>
    /// класс, описывающий один узел дерева
    /// </summary>
    public class TreeNode<NodeInformation>
    {
        /// <summary>
        /// Информация, хранящаяся в узле
        /// </summary>
        public readonly NodeInformation Information;
        /// <summary>
        /// родительский узел по отношению к данному
        /// </summary>
        public readonly TreeNode<NodeInformation> Parent;
        public int NumberOfChildrens { get { return mChildren.Count; } }
        public TreeNode<NodeInformation> GetChildren(int ChildrenNumber)
        {
            return mChildren[ChildrenNumber];
        }
        /// <summary>
        /// Заполняет узел информацией.
        /// </summary>
        /// <param name="nodeInformation">Информация, которая записывается в узел</param>
        /// <param name="parent">Ссылка на родителя, в случае его наличия</param>
        public TreeNode(NodeInformation nodeInformation, TreeNode<NodeInformation> parent = null)
        {
            mChildren = new List<TreeNode<NodeInformation>>();
            Parent = parent;
            if (parent != null)
                parent.AddChildren(this);
            Information = nodeInformation;
        }
        private List<TreeNode<NodeInformation>> mChildren;
        private void AddChildren(TreeNode<NodeInformation> Children)
        {
            mChildren.Add(Children);
        }
    }
}