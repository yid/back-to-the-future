﻿using System;
using Android.App;
using Android.Content;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Views.Animations;
using Conditions;
using Constants;
using Java.Util;
using System.Collections.Generic;
using Android.Graphics;
using Java.IO;

namespace Back_to_the_future
{
    [Activity(ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@android:style/Theme.Black.NoTitleBar")]
    public class Cartoon : Activity
    {
        private Button VK;
        private ImageViewFactory imageViewFactory;
        private DrawingBoard Board;
        private RelativeLayout BackgroundLayout;
        private PictureCondition CurrentPictureCondition;
        private PictureCondition[] Changes;
        private int NumberOfChanges;
        private int CurrentChange;
        private int CurrentNumber;
        private Timer t;
        private Handler ImageViewDrawer;
        private Java.Util.Random rnd = new Java.Util.Random();
        private List<ImageView> Unusefull = new List<ImageView>();

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Cartoon);
            CurrentNumber = 0;
            CurrentChange = 0;

            IParcelable[] temp = Intent.GetParcelableArrayExtra(Const.PictureConditions);
            CurrentPictureCondition = (PictureCondition)temp[0];
            temp[0].Dispose();
            temp[0] = null;
            Changes = new PictureCondition[temp.Length - 1];
            for (int i = 0; i < Changes.Length; i++)
            {
                Changes[i] = (PictureCondition)temp[i + 1];
                temp[i + 1].Dispose();
                temp[i + 1] = null;
            }
            temp = null;
            NumberOfChanges = Intent.GetIntExtra(Const.NumberOfFrames, 2);

            BackgroundLayout = (RelativeLayout)FindViewById(Resource.Id.relativeLayoutForCartoon);
            imageViewFactory = new ImageViewFactory(this);

            BackgroundLayout.Click += BackgroundLayout_Click;

            ImageViewDrawer = new Handler(Draw);

            Board = new DrawingBoard(this, CurrentPictureCondition, Intent.GetIntExtra(Const.LastDeparted, -1), Intent.GetIntExtra(Const.DestinationTime, -1));
            BackgroundLayout.AddView(Board, ViewGroup.LayoutParams.MatchParent);

            VK = new Button(this);
            VK.SetX(10);
            VK.SetY(10);
            VK.LayoutParameters = new RelativeLayout.LayoutParams(80, 80);
            VK.SetBackgroundResource(Android.Resource.Drawable.IcMenuCamera);
            BackgroundLayout.AddView(VK);
            VK.Click += VK_Click;

            StartMovie();
        }

        void BackgroundLayout_Click(object sender, EventArgs e)
        {
            BackgroundLayout.Clickable = false;
            Toast.MakeText(this, "Ничего не делай, Марти, и просто смотри", ToastLength.Long).Show();
        }

        public override void OnBackPressed()
        {
            StopMovie();
        }

        void VK_Click(object sender, EventArgs e)
        {
            VK.Clickable = false;
            BackgroundLayout.DrawingCacheEnabled = true;
            Bitmap b = BackgroundLayout.DrawingCache;
            String pathofBmp = Android.Provider.MediaStore.Images.Media.InsertImage(ContentResolver, b, "title", null);
            BackgroundLayout.DrawingCacheEnabled = false;
            b.Recycle();
            b.Dispose();
            b = null;
            Intent intent = new Intent();
            intent.PutExtra("Result", pathofBmp);
            SetResult(Result.Ok, intent);
        }

        private void Draw(Message msg)
        {
            Board.Invalidate();
            DrawAnimation();
            CurrentNumber++;
            if (CurrentNumber == NumberOfChanges)
            {
                CurrentNumber = 0;
                CurrentChange++;
            }
            if (CurrentChange < Changes.Length)
                CurrentPictureCondition.Add(Changes[CurrentChange]);
            else
                StopMovie();
        }

        private EventHandler<Animation.AnimationEndEventArgs> FinishAnimnation(ImageView v)
        {
            return (object sender, Animation.AnimationEndEventArgs e) =>
            {
                v.Visibility = ViewStates.Invisible;
                Unusefull.Add(v);
            };
        }

        private void StopMovie()
        {
            t.Cancel();
            this.Finish();
        }
        protected override void OnDestroy()
        {
            base.OnDestroy();
            BackgroundLayout.Click -= BackgroundLayout_Click;
            BackgroundLayout.RemoveAllViews();
            BackgroundLayout.Dispose();
            BackgroundLayout = null;
            imageViewFactory.Dispose();
            imageViewFactory = null;
            Board.Dispose();
            Board = null;
            t.Dispose();
            t = null;
            ImageViewDrawer.Dispose();
            ImageViewDrawer = null;
            rnd.Dispose();
            rnd = null;
            for (int i = 0; i < Unusefull.Count; i++)
            {
                Unusefull[i].Drawable.Dispose();
                Unusefull[i].SetImageDrawable(null);
                Unusefull[i].Dispose();
                Unusefull[i] = null;
            }
            VK.Click -= VK_Click;
            VK.Background.Dispose();
            VK.SetBackgroundDrawable(null);
            VK.Dispose();
            VK = null;

            GC.Collect(); // Do not work without this line. It seems that problem with interaction of Java and .NET GC.
        }

        private void StartMovie()
        {
            t = new Timer();
            t.Schedule(new MyTimerTask(ImageViewDrawer), Const.AnimationCreatingFrequency, Const.AnimationCreatingFrequency);
        }

        public void DrawAnimation()
        {
            ImageView AnimatedImageView = null;

            float f = rnd.NextFloat();
            if (f < CurrentPictureCondition.TransportProbability)
            {
                f = rnd.NextFloat();
                if (f < CurrentPictureCondition.TransportIsUfoProbability)
                {
                    AnimatedImageView = imageViewFactory.BuildUFO((int)(Const.TransportAverageTime * (1.5f - CurrentPictureCondition.TransportSpeed)));
                }
                else
                {
                    if (CurrentPictureCondition.TransportSpeed < Const.HorseLevel)
                    {
                        AnimatedImageView = imageViewFactory.BuildHorse((int)(Const.TransportAverageTime * (1.5f - CurrentPictureCondition.TransportSpeed)));
                    }
                    else
                    {
                        if (CurrentPictureCondition.TransportSpeed < Const.CarsLevel)
                        {
                            AnimatedImageView = imageViewFactory.BuildCar((int)(Const.TransportAverageTime * (1.5f - CurrentPictureCondition.TransportSpeed)));
                        }
                        else
                        {
                            if (CurrentPictureCondition.TransportSpeed < Const.FlyingCarsLevel)
                            {
                                AnimatedImageView = imageViewFactory.BuildFlyingCar((int)(Const.TransportAverageTime * (1.5f - CurrentPictureCondition.TransportSpeed)));
                            }
                            else
                            {
                                AnimatedImageView = imageViewFactory.BuildRocket((int)(Const.TransportAverageTime * (1.5f - CurrentPictureCondition.TransportSpeed)));
                            }
                        }
                    }
                }
                AnimatedImageView.Animation.AnimationEnd += FinishAnimnation(AnimatedImageView);
                BackgroundLayout.AddView(AnimatedImageView);
                AnimatedImageView.Animate();
            }

            f = rnd.NextFloat();
            if (f < CurrentPictureCondition.WalkerProbability)
            {
                f = rnd.NextFloat();
                if (f < CurrentPictureCondition.WalkerIsAlienProbability)
                {
                    AnimatedImageView = imageViewFactory.BuildAlien();
                }
                else
                {
                    if (f < CurrentPictureCondition.WalkerIsAlienProbability + CurrentPictureCondition.WalkerIsRobotProbability)
                    {
                        AnimatedImageView = imageViewFactory.BuildRobot();
                    }
                    else
                    {
                        if (f < CurrentPictureCondition.WalkerIsAlienProbability + CurrentPictureCondition.WalkerIsRobotProbability + CurrentPictureCondition.WalkerIsAnimalProbability)
                        {
                            AnimatedImageView = imageViewFactory.BuildAnimal(0.1f + CurrentPictureCondition.AnimalSize);
                        }
                        else
                        {
                            AnimatedImageView = imageViewFactory.BuildHuman();
                        }
                    }
                }
                AnimatedImageView.Animation.AnimationEnd += FinishAnimnation(AnimatedImageView);
                BackgroundLayout.AddView(AnimatedImageView);
                AnimatedImageView.Animate();
            }

            f = 4f * rnd.NextFloat();
            if (f < CurrentPictureCondition.ExplosionProbability)
            {
                AnimatedImageView = imageViewFactory.BuildExplosion();
                AnimatedImageView.Animation.AnimationEnd += FinishAnimnation(AnimatedImageView);
                BackgroundLayout.AddView(AnimatedImageView);
                AnimatedImageView.Animate();
            }
        }
    }

    public class MyTimerTask : TimerTask
    {
        Handler h;
        public MyTimerTask(Handler h)
        {
            this.h = h;
        }
        public override void Run()
        {
            h.SendEmptyMessage(1);
        }
    }
}
