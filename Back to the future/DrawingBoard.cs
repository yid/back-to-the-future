﻿using Android.Content;
using Android.Graphics;
using Android.Views;
using Conditions;
using Constants;
using System.Collections.Generic;

namespace Back_to_the_future
{
    public class DrawingBoard : View
    {
        private Paint p = new Paint();
        private Bitmap HumanBuildingBitmap;
        private Bitmap AlienBuildingBitmap;
        private Bitmap RobotBuildingBitmap;
        private Bitmap TreeBitmap;
        private Bitmap ConsoleBitmap;
        private Bitmap HumansRoof;
        private Bitmap ServerRoof;
        private PictureCondition CurrentPictureCondition;
        private readonly int MaxNumberOfBuildings;
        private Rect BlockSize;
        private Rect ConsoleSize;
        private Rect TreesSize;
        private List<float> TreeCoordinates = new List<float>();
        private Java.Util.Random rnd = new Java.Util.Random();
        private float ScalingKoefficient;
        private int LastTimeDeparted;
        private int DestinationTime;
        public DrawingBoard(Context context, PictureCondition CurrentPictureCondition, int LastTimeDeparted, int DestinationTime) : base(context)
        {
            this.LastTimeDeparted = LastTimeDeparted;
            this.DestinationTime = DestinationTime;
            this.CurrentPictureCondition = CurrentPictureCondition;
            ScalingKoefficient = (Const.ScreenWidth - 4 * Const.DistanceBetweenBuildings) / 3; // ширь одного блока
            ScalingKoefficient = ScalingKoefficient / Const.StandartWidth;
            BlockSize = new Rect(0, 0, (int)(Const.StandartWidth * ScalingKoefficient), (int)(Const.StandartHeight * ScalingKoefficient)); // масштабируем размер блока так, чтобы на экране по ширине помещалось 3 блока
            MaxNumberOfBuildings = Const.ScreenHeight / BlockSize.Height();

            BitmapFactory.Options opt = new BitmapFactory.Options();
            opt.InSampleSize = calculateInSampleSize(BlockSize.Width(), BlockSize.Height());
            AlienBuildingBitmap = BitmapFactory.DecodeResource(context.Resources, Resource.Drawable.AliensHouse, opt);
            HumansRoof = BitmapFactory.DecodeResource(context.Resources, Resource.Drawable.MenHouseRoof, opt);
            ServerRoof = BitmapFactory.DecodeResource(context.Resources, Resource.Drawable.ServerRoof, opt);

            TreesSize = new Rect(0, 0, BlockSize.Bottom, BlockSize.Right);
            opt.InSampleSize = calculateInSampleSize(TreesSize.Width(), TreesSize.Height());
            TreeBitmap = BitmapFactory.DecodeResource(context.Resources, Resource.Drawable.Tree, opt);

            opt.InSampleSize = calculateInSampleSize(BlockSize.Width(), BlockSize.Height());
            opt.InPreferredConfig = Bitmap.Config.Rgb565;
            HumanBuildingBitmap = BitmapFactory.DecodeResource(context.Resources, Resource.Drawable.MenHouse, opt);
            RobotBuildingBitmap = BitmapFactory.DecodeResource(context.Resources, Resource.Drawable.Server, opt);

            ConsoleSize = new Rect(0, 0, BlockSize.Right * 2, BlockSize.Bottom * 2);
            ConsoleSize.OffsetTo(Const.ScreenWidth - 2 * BlockSize.Width(), 0);
            opt.InSampleSize = calculateInSampleSize(ConsoleSize.Width(), ConsoleSize.Height());
            ConsoleBitmap = BitmapFactory.DecodeResource(context.Resources, Resource.Drawable.Console, opt);

            for (int i = 0; i < Const.MaxTreesNumber + 1; i++)
                TreeCoordinates.Add(rnd.NextFloat() * (Const.ScreenWidth - TreesSize.Width()));

            opt.Dispose();
            opt = null;
        }
        public override void Draw(Canvas canvas)
        {
            base.Draw(canvas);

            canvas.DrawARGB(255, 214 + (int)(6f * CurrentPictureCondition.Pollution), 229 - (int)(23f * CurrentPictureCondition.Pollution), 255 - (int)(122 * CurrentPictureCondition.Pollution));
            p.Color = Color.Yellow;
            canvas.DrawCircle(50, 50, 40, p);
            BlockSize.OffsetTo(Const.DistanceBetweenBuildings, canvas.Height);

            for (int i = 0; i < (MaxNumberOfBuildings - 1) * CurrentPictureCondition.NumberOfHumansBuildings; i++)
            {
                BlockSize.Offset(0, -BlockSize.Height());
                canvas.DrawBitmap(HumanBuildingBitmap, null, BlockSize, p);
            }
            if (BlockSize.Top != canvas.Height)
            {
                BlockSize.Offset(0, -BlockSize.Height());
                canvas.DrawBitmap(HumansRoof, null, BlockSize, p);
            }
            BlockSize.OffsetTo(2 * Const.DistanceBetweenBuildings + BlockSize.Width(), canvas.Height);
            for (int i = 0; i < (MaxNumberOfBuildings - 2) * CurrentPictureCondition.NumberOfAliensBuildings; i++)
            {
                BlockSize.Offset(0, -BlockSize.Height());
                canvas.DrawBitmap(AlienBuildingBitmap, null, BlockSize, p);
            }
            BlockSize.OffsetTo(3 * Const.DistanceBetweenBuildings + 2 * BlockSize.Width(), canvas.Height);
            for (int i = 0; i < (MaxNumberOfBuildings - 3) * CurrentPictureCondition.NumberOfRobotsBuildings; i++)
            {
                BlockSize.Offset(0, -BlockSize.Height());
                canvas.DrawBitmap(RobotBuildingBitmap, null, BlockSize, p);
            }
            if (BlockSize.Top != canvas.Height)
            {
                BlockSize.Offset(0, -BlockSize.Height());
                canvas.DrawBitmap(ServerRoof, null, BlockSize, p);
            }

            for (int i = 0; i < Const.MaxTreesNumber * (1.0 - CurrentPictureCondition.Pollution); i++)
            {
                TreesSize.OffsetTo((int)TreeCoordinates[i], canvas.Height - TreesSize.Height());
                canvas.DrawBitmap(TreeBitmap, null, TreesSize, p);
            }

            canvas.DrawBitmap(ConsoleBitmap, null, ConsoleSize, p);

            p.TextSize = 45 * ScalingKoefficient;
            p.Color = Color.Red;
            canvas.DrawText(DestinationTime.ToString(), canvas.Width - 1.2f * BlockSize.Width() + 16f * ScalingKoefficient, ScalingKoefficient * 60f, p);
            p.Color = Color.Green;
            canvas.DrawText(((int)CurrentPictureCondition.Year).ToString(), canvas.Width - 1.2f * BlockSize.Width() + 16f * ScalingKoefficient, ScalingKoefficient * 180f, p);
            p.Color = Color.Yellow;
            canvas.DrawText(LastTimeDeparted.ToString(), canvas.Width - 1.2f * BlockSize.Width() + 16f * ScalingKoefficient, ScalingKoefficient * 300f, p);
        }
        public static int calculateInSampleSize(int reqWidth, int reqHeight)
        {
            // Реальные размеры изображения 
            int height = Const.StandartHeight;
            int width = Const.StandartWidth;
            int inSampleSize = 1;

            if (height > reqHeight || width > reqWidth)
            {

                int halfHeight = height / 2;
                int halfWidth = width / 2;

                // Вычисляем наибольший inSampleSize, который будет кратным двум
                // и оставит полученные размеры больше, чем требуемые 
                while ((halfHeight / inSampleSize) > reqHeight
                       && (halfWidth / inSampleSize) > reqWidth)
                {
                    inSampleSize *= 2;
                }
            }
            return inSampleSize;
        }
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            HumanBuildingBitmap.Recycle();
            AlienBuildingBitmap.Recycle();
            RobotBuildingBitmap.Recycle();
            TreeBitmap.Recycle();
            ConsoleBitmap.Recycle();
            HumansRoof.Recycle();
            ServerRoof.Recycle();
            HumanBuildingBitmap.Dispose();
            AlienBuildingBitmap.Dispose();
            RobotBuildingBitmap.Dispose();
            TreeBitmap.Dispose();
            ConsoleBitmap.Dispose();
            HumansRoof.Dispose();
            ServerRoof.Dispose();
            HumanBuildingBitmap = null;
            AlienBuildingBitmap = null;
            RobotBuildingBitmap = null;
            TreeBitmap = null;
            ConsoleBitmap = null;
            HumansRoof = null;
            ServerRoof = null;
            p.Dispose();
            p = null;
            BlockSize.Dispose();
            BlockSize = null;
            ConsoleSize.Dispose();
            ConsoleSize = null;
            TreesSize.Dispose();
            TreesSize = null;
            rnd.Dispose();
            rnd = null;
        }
    }
}