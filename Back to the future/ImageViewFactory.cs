﻿using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Views.Animations;
using Android.Widget;
using Java.Lang.Ref;
using Constants;

namespace Back_to_the_future
{
    public class ImageViewFactory
    {
        private Java.Util.Random rnd = new Java.Util.Random();
        private Context context;
        private Bitmap Rocket;
        private Bitmap UFO;
        private Bitmap FlyingCar;
        private Bitmap Car;
        private Bitmap Horse;
        private Bitmap Human;
        private Bitmap Alien;
        private Bitmap Robot;
        private Bitmap Animal;
        private Bitmap Explosion;
        private Rect TransportSize;
        private Rect WalkerSize;
        private Rect ExplosionSize;
        private Rect AnimalSize;

        public ImageViewFactory(Context context)
        {
            this.context = context;

            TransportSize = new Rect(0, 0, (int)(Const.ScreenWidth * Const.TransportWidthToFrameWidth), (int)(Const.StandartHeight * Const.ScreenWidth * Const.TransportWidthToFrameWidth / Const.StandartWidth));
            WalkerSize = new Rect(0, 0, (int)(Const.ScreenHeight * Const.WalkerWidthToFrameWidth), (int)(Const.StandartWidth * Const.ScreenHeight * Const.WalkerWidthToFrameWidth / Const.StandartHeight));
            ExplosionSize = new Rect(0, 0, (int)(Const.ScreenWidth * Const.ExplosionWidthToFrameWidth), (int)(Const.ScreenWidth * Const.ExplosionWidthToFrameWidth));
            AnimalSize = new Rect(0, 0, (int)(Const.ScreenWidth * Const.AverageAnimalWidthToFrameWidth), (int)(Const.ScreenWidth * Const.AverageAnimalWidthToFrameWidth * Const.StandartHeight / Const.StandartWidth));

            BitmapFactory.Options opt = new BitmapFactory.Options();
            opt.InSampleSize = DrawingBoard.calculateInSampleSize(TransportSize.Width(), TransportSize.Height());

            Rocket = BitmapFactory.DecodeResource(context.Resources, Resource.Drawable.Rocket, opt);
            UFO = BitmapFactory.DecodeResource(context.Resources, Resource.Drawable.UFO, opt);
            FlyingCar = BitmapFactory.DecodeResource(context.Resources, Resource.Drawable.FlyingCar, opt);
            Car = BitmapFactory.DecodeResource(context.Resources, Resource.Drawable.Car, opt);
            Horse = BitmapFactory.DecodeResource(context.Resources, Resource.Drawable.Horse1, opt);

            opt.InSampleSize = DrawingBoard.calculateInSampleSize(WalkerSize.Width(), WalkerSize.Height());

            Human = BitmapFactory.DecodeResource(context.Resources, Resource.Drawable.Man, opt);
            Alien = BitmapFactory.DecodeResource(context.Resources, Resource.Drawable.Alien, opt);
            Robot = BitmapFactory.DecodeResource(context.Resources, Resource.Drawable.Robot, opt);

            opt.InSampleSize = DrawingBoard.calculateInSampleSize(ExplosionSize.Width(), ExplosionSize.Height());

            Explosion = BitmapFactory.DecodeResource(context.Resources, Resource.Drawable.Bang1, opt);

            opt.InSampleSize = DrawingBoard.calculateInSampleSize(AnimalSize.Width(), AnimalSize.Height());

            Animal = BitmapFactory.DecodeResource(context.Resources, Resource.Drawable.Dino, opt);

            opt.Dispose();
            opt = null;
        }
        public ImageView BuildRocket(int AverageSpeed)
        {
            int RandomHeightLeft = rnd.NextInt(Const.FlyingHeightDispersion + 1);
            int RandomHeightRight = rnd.NextInt(Const.FlyingHeightDispersion + 1);
            bool RandomBool = rnd.NextBoolean();
            ImageView iv = new ImageView(context);
            iv.SetImageDrawable(new MoversDrawable(context, Rocket, RandomBool));
            iv.LayoutParameters = new RelativeLayout.LayoutParams(TransportSize.Width(), TransportSize.Height());
            if (RandomBool)
                iv.Animation = BuildMovingAnimation(Const.ScreenWidth, -TransportSize.Width(), RandomHeightRight, RandomHeightLeft, AverageSpeed + rnd.NextInt(Const.TransportTimeDispersion + 1) * (-1) ^ rnd.NextInt(2));
            else
                iv.Animation = BuildMovingAnimation(-TransportSize.Width(), Const.ScreenWidth, RandomHeightLeft, RandomHeightRight, AverageSpeed + rnd.NextInt(Const.TransportTimeDispersion + 1) * (-1) ^ rnd.NextInt(2));
            return iv;
        }
        public ImageView BuildUFO(int AverageSpeed)
        {
            int RandomHeightLeft = rnd.NextInt(Const.FlyingHeightDispersion + 1);
            int RandomHeightRight = rnd.NextInt(Const.FlyingHeightDispersion + 1);
            bool RandomBool = rnd.NextBoolean();
            ImageView iv = new ImageView(context);
            iv.SetImageDrawable(new MoversDrawable(context, UFO, RandomBool));
            iv.LayoutParameters = new RelativeLayout.LayoutParams(TransportSize.Width(), TransportSize.Height());
            if (RandomBool)
                iv.Animation = BuildMovingAnimation(Const.ScreenWidth, -TransportSize.Width(), RandomHeightRight, RandomHeightLeft, AverageSpeed + rnd.NextInt(Const.TransportTimeDispersion + 1) * (-1) ^ rnd.NextInt(2));
            else
                iv.Animation = BuildMovingAnimation(-TransportSize.Width(), Const.ScreenWidth, RandomHeightLeft, RandomHeightRight, AverageSpeed + rnd.NextInt(Const.TransportTimeDispersion + 1) * (-1) ^ rnd.NextInt(2));
            return iv;
        }
        public ImageView BuildFlyingCar(int AverageSpeed)
        {
            int RandomHeightLeft = rnd.NextInt(Const.FlyingHeightDispersion + 1);
            int RandomHeightRight = rnd.NextInt(Const.FlyingHeightDispersion + 1);
            bool RandomBool = rnd.NextBoolean();
            ImageView iv = new ImageView(context);
            iv.SetImageDrawable(new MoversDrawable(context, FlyingCar, RandomBool));
            iv.LayoutParameters = new RelativeLayout.LayoutParams(TransportSize.Width(), TransportSize.Height());
            if (RandomBool)
                iv.Animation = BuildMovingAnimation(Const.ScreenWidth, -TransportSize.Width(), Const.ScreenHeight / 2 + RandomHeightRight, Const.ScreenHeight / 2 + RandomHeightLeft, AverageSpeed + rnd.NextInt(Const.TransportTimeDispersion + 1) * (-1) ^ rnd.NextInt(2));
            else
                iv.Animation = BuildMovingAnimation(-TransportSize.Width(), Const.ScreenWidth, Const.ScreenHeight / 2 + RandomHeightLeft, Const.ScreenHeight / 2 + RandomHeightRight, AverageSpeed + rnd.NextInt(Const.TransportTimeDispersion + 1) * (-1) ^ rnd.NextInt(2));
            return iv;
        }
        public ImageView BuildCar(int AverageSpeed)
        {
            bool RandomBool = rnd.NextBoolean();
            ImageView iv = new ImageView(context);
            iv.SetImageDrawable(new MoversDrawable(context, Car, RandomBool));
            iv.LayoutParameters = new RelativeLayout.LayoutParams(TransportSize.Width(), TransportSize.Height());
            if (RandomBool)
                iv.Animation = BuildMovingAnimation(Const.ScreenWidth, -TransportSize.Width(), Const.ScreenHeight - TransportSize.Height(), Const.ScreenHeight - TransportSize.Height(), AverageSpeed + rnd.NextInt(Const.TransportTimeDispersion + 1) * (-1) ^ rnd.NextInt(2));
            else
                iv.Animation = BuildMovingAnimation(-TransportSize.Width(), Const.ScreenWidth, Const.ScreenHeight - TransportSize.Height(), Const.ScreenHeight - TransportSize.Height(), AverageSpeed + rnd.NextInt(Const.TransportTimeDispersion + 1) * (-1) ^ rnd.NextInt(2));
            return iv;
        }
        public ImageView BuildHorse(int AverageSpeed)
        {
            bool RandomBool = rnd.NextBoolean();
            ImageView iv = new ImageView(context);
            iv.SetImageDrawable(new MoversDrawable(context, Horse, RandomBool));
            iv.LayoutParameters = new RelativeLayout.LayoutParams(TransportSize.Width(), TransportSize.Height());
            if (RandomBool)
                iv.Animation = BuildMovingAnimation(Const.ScreenWidth, -TransportSize.Width(), Const.ScreenHeight - TransportSize.Height(), Const.ScreenHeight - TransportSize.Height(), AverageSpeed + rnd.NextInt(Const.TransportTimeDispersion + 1) * (-1) ^ rnd.NextInt(2));
            else
                iv.Animation = BuildMovingAnimation(-TransportSize.Width(), Const.ScreenWidth, Const.ScreenHeight - TransportSize.Height(), Const.ScreenHeight - TransportSize.Height(), AverageSpeed + rnd.NextInt(Const.TransportTimeDispersion + 1) * (-1) ^ rnd.NextInt(2));
            return iv;
        }
        public ImageView BuildHuman()
        {
            bool RandomBool = rnd.NextBoolean();
            ImageView iv = new ImageView(context);
            iv.SetImageDrawable(new MoversDrawable(context, Human, RandomBool));
            iv.LayoutParameters = new RelativeLayout.LayoutParams(WalkerSize.Width(), WalkerSize.Height());
            if (RandomBool)
                iv.Animation = BuildMovingAnimation(Const.ScreenWidth, -WalkerSize.Width(), Const.ScreenHeight - WalkerSize.Height(), Const.ScreenHeight - WalkerSize.Height(), Const.WalkerAverageTime + rnd.NextInt(Const.WalkerTimeDispersion + 1) * (-1) ^ rnd.NextInt(2));
            else
                iv.Animation = BuildMovingAnimation(-WalkerSize.Width(), Const.ScreenWidth, Const.ScreenHeight - WalkerSize.Height(), Const.ScreenHeight - WalkerSize.Height(), Const.WalkerAverageTime + rnd.NextInt(Const.WalkerTimeDispersion + 1) * (-1) ^ rnd.NextInt(2));
            return iv;
        }
        public ImageView BuildAlien()
        {
            bool RandomBool = rnd.NextBoolean();
            ImageView iv = new ImageView(context);
            iv.SetImageDrawable(new MoversDrawable(context, Alien, RandomBool));
            iv.LayoutParameters = new RelativeLayout.LayoutParams(WalkerSize.Width(), WalkerSize.Height());
            if (RandomBool)
                iv.Animation = BuildMovingAnimation(Const.ScreenWidth, -WalkerSize.Width(), Const.ScreenHeight - WalkerSize.Height(), Const.ScreenHeight - WalkerSize.Height(), Const.WalkerAverageTime + rnd.NextInt(Const.WalkerTimeDispersion + 1) * (-1) ^ rnd.NextInt(2));
            else
                iv.Animation = BuildMovingAnimation(-WalkerSize.Width(), Const.ScreenWidth, Const.ScreenHeight - WalkerSize.Height(), Const.ScreenHeight - WalkerSize.Height(), Const.WalkerAverageTime + rnd.NextInt(Const.WalkerTimeDispersion + 1) * (-1) ^ rnd.NextInt(2));
            return iv;
        }
        public ImageView BuildRobot()
        {
            bool RandomBool = rnd.NextBoolean();
            ImageView iv = new ImageView(context);
            iv.SetImageDrawable(new MoversDrawable(context, Robot, RandomBool));
            iv.LayoutParameters = new RelativeLayout.LayoutParams(WalkerSize.Width(), WalkerSize.Height());
            if (RandomBool)
                iv.Animation = BuildMovingAnimation(Const.ScreenWidth, -WalkerSize.Width(), Const.ScreenHeight - WalkerSize.Height(), Const.ScreenHeight - WalkerSize.Height(), Const.WalkerAverageTime + rnd.NextInt(Const.WalkerTimeDispersion + 1) * (-1) ^ rnd.NextInt(2));
            else
                iv.Animation = BuildMovingAnimation(-WalkerSize.Width(), Const.ScreenWidth, Const.ScreenHeight - WalkerSize.Height(), Const.ScreenHeight - WalkerSize.Height(), Const.WalkerAverageTime + rnd.NextInt(Const.WalkerTimeDispersion + 1) * (-1) ^ rnd.NextInt(2));
            return iv;
        }
        public ImageView BuildAnimal(float Size)
        {
            int RandomHeightLeft = rnd.NextInt(Const.FlyingHeightDispersion + 1);
            int RandomHeightRight = rnd.NextInt(Const.FlyingHeightDispersion + 1);
            bool RandomBool = rnd.NextBoolean();
            ImageView iv = new ImageView(context);
            iv.SetImageDrawable(new MoversDrawable(context, Animal, !RandomBool));
            iv.LayoutParameters = new RelativeLayout.LayoutParams(AnimalSize.Width(), AnimalSize.Height());
            if (RandomBool)
                iv.Animation = BuildMovingAnimation(Const.ScreenWidth, -AnimalSize.Width(), Const.ScreenHeight / 2 + RandomHeightRight, Const.ScreenHeight / 2 + RandomHeightLeft, Const.WalkerAverageTime + rnd.NextInt(Const.WalkerTimeDispersion + 1) * (-1) ^ rnd.NextInt(2));
            else
                iv.Animation = BuildMovingAnimation(-AnimalSize.Width(), Const.ScreenWidth, Const.ScreenHeight / 2 + RandomHeightLeft, Const.ScreenHeight / 2 + RandomHeightRight, Const.WalkerAverageTime + rnd.NextInt(Const.WalkerTimeDispersion + 1) * (-1) ^ rnd.NextInt(2));
            return iv;
        }
        public ImageView BuildExplosion()
        {
            ImageView iv = new ImageView(context);
            iv.SetImageDrawable(new MoversDrawable(context, Explosion, false));
            iv.LayoutParameters = new RelativeLayout.LayoutParams(ExplosionSize.Width(), ExplosionSize.Height());
            iv.SetX(0.5f * Const.ScreenWidth * (1f + rnd.NextFloat()) - ExplosionSize.Width());
            iv.SetY(0.5f * Const.ScreenHeight * (1f + rnd.NextFloat()) - ExplosionSize.Height());
            iv.Animation = BuildScaleAnimation(0f, 1f, 0f, 1f, iv.GetX() + ExplosionSize.Width() / 2f, iv.GetY() + ExplosionSize.Height() / 2f, Const.ExplosionTime);
            return iv;
        }

        private Animation BuildMovingAnimation(float FromX, float ToX, float FromY, float ToY, long Duration)
        {
            TranslateAnimation ta = new TranslateAnimation(FromX, ToX, FromY, ToY);
            ta.Duration = Duration;
            return ta;
        }
        private Animation BuildScaleAnimation(float FromX, float ToX, float FromY, float ToY, float CenterX, float CenterY, long Duration)
        {
            ScaleAnimation ta = new ScaleAnimation(FromX, ToX, FromY, ToY, CenterX, CenterY);
            ta.Duration = Duration;
            return ta;
        }
        public void Dispose()
        {
            Rocket.Recycle();
            UFO.Recycle();
            FlyingCar.Recycle();
            Car.Recycle();
            Horse.Recycle();
            Human.Recycle();
            Alien.Recycle();
            Robot.Recycle();
            Animal.Recycle();
            Explosion.Recycle();
            /*
            Rocket.Dispose();
            UFO.Dispose();
            FlyingCar.Dispose();
            Car.Dispose();
            Horse.Dispose();
            Human.Dispose();
            Alien.Dispose();
            Robot.Dispose();
            Animal.Dispose();
            Explosion.Dispose();
            Rocket = null;
            UFO = null;
            FlyingCar = null;
            Car = null;
            Horse = null;
            Human = null;
            Alien = null;
            Robot = null;
            Animal = null;
            Explosion = null;
            TransportSize.Dispose();
            WalkerSize.Dispose();
            ExplosionSize.Dispose();
            AnimalSize.Dispose();
            TransportSize = null;
            ExplosionSize = null;
            WalkerSize = null;
            AnimalSize = null;
            If uncomment direct disposing, program crashes. It may be connected with interaction of Java and .NET GC.
            */
            rnd.Dispose();
            rnd = null;
        }
    }
    class MoversDrawable : Drawable
    {
        private Paint p = new Paint();
        private Bitmap bitmap;
        bool Mirrored;

        public MoversDrawable(Context context, Bitmap bmp, bool Mirrored)
        {
            bitmap = bmp;
            this.Mirrored = Mirrored;
        }
        public override void Draw(Canvas canvas)
        {
            if (Mirrored)
                canvas.Scale(-1, 1);
            canvas.DrawBitmap(bitmap, null, canvas.ClipBounds, p);
        }
        public override void SetAlpha(int alpha)
        {
        }
        public override int Opacity
        {
            get
            {
                return 0;
            }
        }
        public override void SetColorFilter(ColorFilter cf)
        {
        }
        protected override void OnBoundsChange(Rect bounds)
        {
            base.OnBoundsChange(bounds);
        }
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            p.Dispose();
            p = null;
            bitmap = null;
        }
    }
}

