﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Constants;
using Android.Views.Animations;
using System;

namespace Back_to_the_future
{
    [Activity(Label = "Back to the future", Theme = "@android:style/Theme.Translucent.NoTitleBar")]
    public class Info : Activity
    {
        private ScrollView svForInfo;
        private ImageView ivEnvelope;
        private ImageView ivEnvelope2;
        private ImageView ivInfo;
        private Button bBack;
        private FrameLayout flForInfo;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Info);

            ivEnvelope2 = (ImageView)FindViewById(Resource.Id.imageViewEnvelope2);
            ivEnvelope = (ImageView)FindViewById(Resource.Id.imageViewEnvelope);
            svForInfo = (ScrollView)FindViewById(Resource.Id.scrollViewForInfo);
            flForInfo = (FrameLayout)FindViewById(Resource.Id.frameLayoutForInfo);
            ivInfo = (ImageView)FindViewById(Resource.Id.imageViewInfo);
            bBack = (Button)FindViewById(Resource.Id.buttonBackInfo);

            ivInfo.SetPadding(3, Const.ScreenHeight / 2, 3, Const.ScreenHeight / 2);
            switch (Intent.GetIntExtra(Const.TypeOfInfo, -1))
            {
                case 0:
                    ivInfo.SetImageResource(Resource.Drawable.Instructions);
                    break;
                case 1:
                    ivInfo.SetImageResource(Resource.Drawable.About);
                    break;
                case 2:
                    ivInfo.SetImageResource(Resource.Drawable.Prehistory);
                    break;
            }
            bBack.Click += BBack_Click;
        }
        protected override void OnDestroy()
        {
            base.OnDestroy();
            flForInfo.RemoveAllViews();
            flForInfo.Dispose();
            flForInfo = null;
            ivEnvelope.Drawable.Dispose();
            ivEnvelope.SetImageDrawable(null);
            ivEnvelope.Dispose();
            ivEnvelope = null;
            ivEnvelope2.Drawable.Dispose();
            ivEnvelope2.SetImageDrawable(null);
            ivEnvelope2.Dispose();
            ivEnvelope2 = null;
            bBack.Click -= BBack_Click;
            bBack.Dispose();
            bBack = null;
            svForInfo.RemoveAllViews();
            svForInfo.Dispose();
            svForInfo = null;
            ivInfo.Drawable.Dispose();
            ivInfo.SetImageDrawable(null);
            ivInfo.Dispose();
            ivInfo = null;
        }
        void BBack_Click(object sender, System.EventArgs e)
        {
            Finish();
        }
        protected override void OnResume()
        {
            base.OnResume();

            flForInfo.StartAnimation(AnimationUtils.LoadAnimation(this, Resource.Animation.FromDownToUp));
        }
    }
}

