﻿using Android.App;

namespace Constants
{
    class Const
    {
        public static int ScreenHeight = Application.Context.Resources.DisplayMetrics.HeightPixels - Application.Context.Resources.GetDimensionPixelSize(Application.Context.Resources.GetIdentifier("status_bar_height", "dimen", "android"));
        public static int ScreenWidth = Application.Context.Resources.DisplayMetrics.WidthPixels;
        public const string TypeOfInfo = "TypeOfInfo";
        public const string TypeOfGame = "TypeOfGame";
        public const string SavedGame = "SavedGame";
        public const string PictureConditions = "PictureConditions";
        public const string NumberOfFrames = "NumberOfFrames";
        public const string LastDeparted = "LastDeparted";
        public const string DestinationTime = "DestinationTime";
        public const int NumberOfStartNode = 17;
        public const int CartoonLength = 400;
        public const int AnimationCreatingFrequency = 100;
        public const int DistanceBetweenBuildings = 10;
        public const int TransportAverageTime = 4000;
        public const int TransportTimeDispersion = 1000;
        public const int WalkerAverageTime = 7000;
        public const int WalkerTimeDispersion = 2000;
        public const int FlyingHeightDispersion = 300;
        public const float TransportWidthToFrameWidth = 0.3f;
        public const float ExplosionWidthToFrameWidth = 0.3f;
        public const float WalkerWidthToFrameWidth = 0.06f;
        public const float AverageAnimalWidthToFrameWidth = 0.4f;
        public const int StandartHeight = 180;
        public const int StandartWidth = 320;
        public const float HorseLevel = 0.3f;
        public const float CarsLevel = 0.6f;
        public const float FlyingCarsLevel = 0.8f;
        public const int ExplosionTime = 2000;
        public const int MaxTreesNumber = 30;
    }
}