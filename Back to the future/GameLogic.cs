﻿using System.Collections.Generic;
using System;
using Tree;
using Android.Content;
using Back_to_the_future;

namespace GameLogic
{
    public class TheGame
    {
        /// <summary>
        /// текущее положение игрока (узел) на дереве
        /// </summary>
        public int PresentPosition
        {
            get; private set;
        }
        /// <summary>
        /// функция, возвращающая путь между настоящим положением и заданным в случае путешествия в прошлое и путь между текущим положением и случайным в случае путешествия в будущее
        /// </summary>
        /// <param name="NumberOfTargetNode">В случае путешествия в прошлое - номер конечного узла, а в случае путешествия в будущее - номер узла, следующего за текущим (остальные выберутся случайным образом)</param>
        /// <returns>коллекция номеров промежуточных узлов</returns>
        public List<int> Travel(int NumberOfTargetNode)
        {
            List<int> Way = new List<int>();
            Way.Add(Tree[PresentPosition].Information);
            TreeNode<int> TempNode;
            if (NumberOfTargetNode > PresentPosition)
            {
                Random rnd = new Random();
                TempNode = Tree[NumberOfTargetNode];
                Way.Add(TempNode.Information);
                while (TempNode.NumberOfChildrens != 0)
                {
                    TempNode = TempNode.GetChildren(rnd.Next(TempNode.NumberOfChildrens));
                    Way.Add(TempNode.Information);
                }
                PresentPosition = TempNode.Information;
            }
            else
            {
                TempNode = Tree[PresentPosition].Parent;
                Way.Add(TempNode.Information);
                while (NumberOfTargetNode != TempNode.Information)
                {
                    TempNode = TempNode.Parent;
                    Way.Add(TempNode.Information);
                }
                PresentPosition = NumberOfTargetNode;
            }
            return Way;
        }
        /// <summary>
        /// Выдает информацию о текущем времени
        /// </summary>
        /// <returns>Информация о текущем времени в виде коллекции из int следующего формата: номер текущего узла, является ли данный узел конечным (1 или 0), затем набор номеров смежных узлов</returns>
        public List<int> GetCurrentPositionInformation()
        {
            List<int> Options = new List<int>();
            Options.Add(Tree[PresentPosition].Information);
            if (Tree[PresentPosition].NumberOfChildrens == 0)
            {
                Options.Add(1);
                TreeNode<int> TempNode = Tree[PresentPosition].Parent;
                Options.Add(TempNode.Information);
                do
                {
                    TempNode = TempNode.Parent;
                    Options.Add(TempNode.Information);
                }
                while (TempNode.Parent != null);
            }
            else
            {
                Options.Add(0);
                for (int i = 0; i < Tree[PresentPosition].NumberOfChildrens; i++)
                {
                    Options.Add(Tree[PresentPosition].GetChildren(i).Information);
                }
            }
            return Options;
        }
        public TheGame(Context context, int StartPosition = Constants.Const.NumberOfStartNode)
        {
            Tree = new List<TreeNode<int>>();
            int[] Parrents = context.Resources.GetIntArray(Resource.Array.Parrents);
            Tree.Add(new TreeNode<int>(0));
            for (int i = 1; i < Parrents.Length; i++)
            {
                Tree.Add(new TreeNode<int>(i, Tree[Parrents[i]]));
            }
            PresentPosition = StartPosition;
        }
        private List<TreeNode<int>> Tree;
    }
}