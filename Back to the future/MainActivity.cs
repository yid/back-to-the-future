﻿using System;
using Android.App;
using Android.Content;
using Android.Widget;
using Android.OS;
using Constants;

namespace Back_to_the_future
{
    [Activity(MainLauncher = true, ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@android:style/Theme.Black.NoTitleBar")]
    public class MainActivity : Activity
    {
        private ImageView logo;
        private ImageView ivNew;
        private ImageView ivLoad;
        private ImageView ivHelp;
        private ImageView ivAbout;
        private ImageView ivExit;
        private LinearLayout llForMenu;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Main);
            logo = (ImageView)FindViewById(Resource.Id.imageView_main_logo);
            ivNew = (ImageView)FindViewById(Resource.Id.imageViewMenuNew);
            ivLoad = (ImageView)FindViewById(Resource.Id.imageViewMenuLoad);
            ivHelp = (ImageView)FindViewById(Resource.Id.imageViewMenuHelp);
            ivAbout = (ImageView)FindViewById(Resource.Id.imageViewMenuAbout);
            ivExit = (ImageView)FindViewById(Resource.Id.imageViewMenuExit);
            llForMenu = (LinearLayout)FindViewById(Resource.Id.linearLayoutForMenu);
            ivNew.Click += IvNew_Click;
            ivLoad.Click += IvLoad_Click;
            ivHelp.Click += IvHelp_Click;
            ivAbout.Click += IvAbout_Click;
            ivExit.Click += IvExit_Click;
        }
        protected override void OnDestroy()
        {
            base.OnDestroy();
            llForMenu.RemoveAllViews();
            llForMenu.Dispose();
            llForMenu = null;
            logo.Drawable.Dispose();
            logo.SetImageDrawable(null);
            logo.Dispose();
            logo = null;
            ivNew.Click -= IvNew_Click;
            ivNew.Drawable.Dispose();
            ivNew.SetImageDrawable(null);
            ivNew.Dispose();
            ivNew = null;
            ivLoad.Click -= IvLoad_Click;
            ivLoad.Drawable.Dispose();
            ivLoad.SetImageDrawable(null);
            ivLoad.Dispose();
            ivLoad = null;
            ivHelp.Click -= IvHelp_Click;
            ivHelp.Drawable.Dispose();
            ivHelp.SetImageDrawable(null);
            ivHelp.Dispose();
            ivHelp = null;
            ivAbout.Click -= IvAbout_Click;
            ivAbout.Drawable.Dispose();
            ivAbout.SetImageDrawable(null);
            ivAbout.Dispose();
            ivAbout = null;
            ivExit.Click -= IvExit_Click;
            ivExit.Drawable.Dispose();
            ivExit.SetImageDrawable(null);
            ivExit.Dispose();
            ivExit = null;
        }

        void IvExit_Click(object sender, EventArgs e)
        {
            Finish();
        }

        void IvAbout_Click(object sender, EventArgs e)
        {
            OpenInfo(1);
        }

        void IvHelp_Click(object sender, EventArgs e)
        {
            OpenInfo(0);
        }

        void IvLoad_Click(object sender, EventArgs e)
        {
            StartGame(1);
        }

        void IvNew_Click(object sender, EventArgs e)
        {
            StartGame(0);
        }
        private void OpenInfo(int n)
        {
            Intent intent = new Intent(this, typeof(Info));
            intent.PutExtra(Const.TypeOfInfo, n);
            StartActivity(intent);
        }
        private void StartGame(int n)
        {
            Intent intent = new Intent(this, typeof(Game));
            intent.PutExtra(Const.TypeOfGame, n);
            StartActivity(intent);
        }
    }
}