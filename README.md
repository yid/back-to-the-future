# Back to the future

The android game based on "Back to the future" movie. The project-finalist of I GS Group all-Russian programming competition 2015.

Have you ever thought why our 2015 year was not like in "Back to the future"? Where is flying cars, skateboards and holograms? It seems that time line
made a wrong turn somewhere. Our game suggests you to find that crucial moment and fix it. You will explore a tree of alternative realities to find the "true" reality. 

The program written with C# using Xamarin framework. You can upgrade the game by adding extra nodes to the events tree or use as template for your own tree-based quest application.
But be carefull with using resources of this application, most of them have license restrictions (chech license information of this repo).
It was our first Android application and now we see that the code is not very clear and effective. Sorry for it.

Program was written with Visual Studio and may be easy compiled and launced from it by this instruction (tested on Visual Studio 2019 Preview):
1.  Install Visual Studio with Xamarin extension.
2.  Clone this repository.
3.  Open .sln file. Here may be a problem that this project not supported, but for me it was solved by manually reloading project (right click to project in Solution Exploreer->Reload)
4.  Run on real or virtual device right from Visual Studio. Here may be a problem that Android Device Manager and SDK Manager require admin previleges to install required packages. For me it was solved by running Visual Studio with admin rights.

To tune the app you can try folowing:
*  In the file Constants.cs you can tune cartoon length, how often animated object generating, walkers and transport speed and size and other cartoon parameters. Also you can choose start node number. We think that constant's names saying what they affecting for.
*  In the file Resources/values/arrays.xml you can add extra nodes or tune node parameters. 

Instruction to add new node:
1.  In the end of <string-array name="Descriptions"> add description of new node (we have used format year/n desription). This description will be shown in description field of current time.
2.  In the end of <string-array name="Links"> add description of transition from parent of your new node to this node. This description will be shown on one of buttons when you visit parent of your new node.
3.  In the end of <string-array name="YearConditions"> add parameters of your new time (all number except year shold be from 0 to 255) in format <item>year,number of humans,number of aliens,industry level, number of dinosurs,pollution level,transport level,war level</item>. This parameters will affect on number and type of animated objects, size of buildings and number of trees.
4.  In the end of <array name="NodePictures"> add name of pictyre corespondind to new node (this picture should be placed to Resources/drawable dir).
5.  In the end of <integer-array name="Parrents"> add number of parent node for your new node (the number of node coresponds to the number in described arrays).
6.  All done, the new node will be placed in the tree.

# License

Программа, содержащаяся в этом репозитории позиционируется как некомерческая игровая пародия основанная на фильме ["Назад в будущее"](https://ru.wikipedia.org/wiki/%D0%9D%D0%B0%D0%B7%D0%B0%D0%B4_%D0%B2_%D0%B1%D1%83%D0%B4%D1%83%D1%89%D0%B5%D0%B5_(%D1%81%D0%B5%D1%80%D0%B8%D1%8F_%D1%84%D0%B8%D0%BB%D1%8C%D0%BC%D0%BE%D0%B2)) поставленном режиссёром Робертом Земекисом.
Образы, имена и идеи из этого фильма используются в рамках [статьи 1274 ГК РФ, пункт 4](http://www.consultant.ru/document/cons_doc_LAW_64629/84bbd636598a59112a4fe972432343dd4f51da1d/).

Также в программе используются следующие произведения:
1.  Фрагмент фотографии "Saqqara Pyramid in Egypt" (файл node00.jpg), автор David Mateos García, [ссылка на источник](https://ru.wikipedia.org/wiki/%D0%A4%D0%B0%D0%B9%D0%BB:Saqqara.jpg). Распространяется по лицензии [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/deed.ru).
2.  Фотография пустыни (файл node01.jpg), [ссылка на источник](https://pixabay.com/ru/%D0%B0%D1%84%D1%80%D0%B8%D0%BA%D0%B0-%D0%BD%D0%B0%D0%BC%D0%B8%D0%B1%D0%B8%D1%8F-%D0%BF%D0%B5%D0%B9%D0%B7%D0%B0%D0%B6-%D0%BF%D1%83%D1%81%D1%82%D1%8B%D0%BD%D1%8F-%D0%BD%D0%B0%D0%BC%D0%B8%D0%B1-1170032/). Лицензия [Pixabay License](https://pixabay.com/ru/service/license/).
3.  Фотография "Supervivència a Cambotja" (файл node02.jpg), автор [Anna](https://www.flickr.com/photos/art_es_anna/), 2006, [ссылка на источник](https://www.flickr.com/photos/61219542@N00/318982699/). Распространяется по лицензии [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/deed.ru).
4.  Мем "Наверное не тот год" (файл node03.jpg), [ссылка на источник](http://www.kukharski.net/dayphoto/marty-mcfly-navernoe-ne-tot-god.html).
5.  Фрагмент фото "Leonard Nimoy and William Shatner as Mr. Spock and Captain Kirk from the television program Star Trek" (файл node05.jpg),  © 1968 NBC Television [ссылка на источник](https://en.wikipedia.org/wiki/File:Leonard_Nimoy_William_Shatner_Star_Trek_1968.JPG), public domain of USA.
6.  Фрагмент фото "Инопланетяне" (файл node06.jpg), [ссылка на источник](https://pxhere.com/ru/photo/984874). [Распространяется по лицензии Creative Commons CC0 1.0 Universal (CC0 1.0) Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
7.  Фото "Заброшенный город Припять" (файл node07.jpg), [ссылка на источник](https://pixabay.com/ru/%D0%BF%D1%80%D0%B8%D0%BF%D1%8F%D1%82%D1%8C-%D1%87%D0%B5%D1%80%D0%BD%D0%BE%D0%B1%D1%8B%D0%BB%D1%8C-%D1%87%D0%B0%D1%8D%D1%81-1366163/). Лицензия [Pixabay License](https://pixabay.com/ru/service/license/).
8.  Фото "Динозавры в городе" (файл node08.jpg), [ссылка на иточник](https://pxhere.com/ru/photo/750798). [Распространяется по лицензии Creative Commons CC0 1.0 Universal (CC0 1.0) Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
9.  Фото "Толпа на улице" (файл node09.jpg), [ссылка на источник](https://pxhere.com/ru/photo/1413082). [Распространяется по лицензии Creative Commons CC0 1.0 Universal (CC0 1.0) Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
10. Автомобиль Delorean (файл delorean.png), [ссылка на источник ](https://pixabay.com/ru/delorean-%D0%B0%D0%B2%D1%82%D0%BE%D0%BC%D0%BE%D0%B1%D0%B8%D0%BB%D1%8C-%D0%BC%D0%B0%D1%88%D0%B8%D0%BD%D0%B0-%D0%B2%D1%80%D0%B5%D0%BC%D0%B5%D0%BD%D0%B8-38103/). Лицензия [Pixabay License](https://pixabay.com/ru/service/license/).
11. Лого (файл Back_to_the_Future_film_series_logo.png), [ссылка на источник](https://commons.wikimedia.org/wiki/File:Back_to_the_Future_film_series_logo.png), согласно информации по ссылке, на защищается авторским правом. Файл Icon.png создан на основе этого же файла.
12. Фото "Pyke(Marty Mcfly) - Back to the Future" (файл node10.jpg), автор [Ricardo](https://www.flickr.com/people/41773804@N08), 2015, [ссылка на источник](https://www.flickr.com/photos/riyagi/20232614198/). Распространяется по лицензии [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/deed.ru).
13. Фото "Робот и природа" (файл node11.jpg), [ссылка на источник](https://pixabay.com/ru/%D0%B7%D0%B5%D0%BB%D0%B5%D0%BD%D1%8B%D0%B9-%D1%80%D0%B0%D1%81%D1%82%D0%B5%D0%BD%D0%B8%D0%B9-%D0%B4%D0%B5%D1%80%D0%B5%D0%B2%D1%8C%D1%8F-%D0%BB%D0%B5%D1%81%D0%B0-2600698/). Лицензия [Pixabay License](https://pixabay.com/ru/service/license/).
14. Фото "Some of the many Mr. Smith mannequins used in the sequel for The Matrix" (файл node12.jpg), автор [Marcin Wichary](https://www.flickr.com/photos/mwichary/), 2008, [ссылка на источник](https://www.flickr.com/photos/mwichary/2862323128). Распространяется по лицензии [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/deed.ru).
15. Фото "Город в зелени" (файл node13.jpg), [ссылка на источник](https://pxhere.com/ru/photo/1402520). [Распространяется по лицензии Creative Commons CC0 1.0 Universal (CC0 1.0) Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
16. Фото "Первобытный человек" (файл node14.jpg), [ссылка на источник](https://pixabay.com/ru/%D0%BF%D0%B5%D1%80%D0%B2%D0%BE%D0%B1%D1%8B%D1%82%D0%BD%D1%8B%D0%B9-%D1%87%D0%B5%D0%BB%D0%BE%D0%B2%D0%B5%D0%BA-%D0%B4%D1%80%D0%B5%D0%B2%D0%BD%D0%B8%D1%85-%D1%81%D1%82%D0%B0%D1%82%D1%83%D1%8F-710627/). Лицензия [Pixabay License](https://pixabay.com/ru/service/license/).
17. Фото "Roman Army & Chariot Experience, Hippodrome, Jerash, Jordan" (файл node16.jpg), автор yeowatzup, 2010, [ссылка на источник](https://commons.wikimedia.org/wiki/File:Roman_Army_%26_Chariot_Experience,_Hippodrome,_Jerash,_Jordan_(5072679364).jpg). Распространяется по лицензии [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/deed.ru).
18. Картина "Христофор Колумб прибывает в Америку" (файл node17.jpg), published by the Prang Educational Co., 1893. 40802Y U.S. Copyright Office. [ссылка на источник](https://sw.m.wikipedia.org/wiki/Picha:Columbus_Taking_Possession.jpg). This work is in the public domain in the United States.
19. Фото "Crookes tube xray experiment" (файл node18.jpg), автор William J. Morton, 1896, [ссылка на источник](https://ru.wikipedia.org/wiki/%D0%A4%D0%B0%D0%B9%D0%BB:Crookes_tube_xray_experiment.jpg). Public domain - published in USA before 1923.
20. Изображение "Гагарин и звезды" (файл node19.jpg), [ссылка на источник](https://pxhere.com/ru/photo/287010). Распространяется по лицензии [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/deed.ru)
21. Фрагмент Изображения "Инопланетянин" (файл node20.jpg), [ссылка на источник](https://pixabay.com/ru/%D0%BD%D0%BB%D0%BE-%D0%B8%D0%BD%D0%BE%D1%81%D1%82%D1%80%D0%B0%D0%BD%D0%B5%D1%86-%D0%B3%D0%B0%D0%B9-pozaziemianin-2413965/). Лицензия [Pixabay License](https://pixabay.com/ru/service/license/).
22. Изображение "Танец смерти" (файл node21.jpg), [ссылка на источник](https://pixabay.com/ru/%D1%82%D0%B0%D0%BD%D0%B5%D1%86-%D1%81%D0%BC%D0%B5%D1%80%D1%82%D1%8C-%D0%BC%D1%80%D0%B0%D1%87%D0%BD%D1%8B%D0%B9-%D1%87%D1%83%D0%BC%D0%B0-%D0%BB%D1%8E%D0%B4%D0%B8-161444/). Лицензия [Pixabay License](https://pixabay.com/ru/service/license/).
23. Фото "William Notman studios - Sitting Bull and Buffalo Bill" (файл node22.jpg), автор William Notman studios, Montreal, Quebec, Canada, 1885, [ссылка на источник](https://ru.wikipedia.org/wiki/%D0%A4%D0%B0%D0%B9%D0%BB:William_Notman_studios_-_Sitting_Bull_and_Buffalo_Bill_(1895)_edit.jpg), [ссылка на оригинальный источник](http://www.loc.gov/pictures/item/2007675831/). Public domain in Canada and USA - published before 1924.
24. Изображение "Флаг земли" (файл node23.jpg), [ссылка на источник](https://pixabay.com/ru/%D0%B0%D0%BC%D0%B5%D1%80%D0%B8%D0%BA%D0%B0-%D0%B7%D0%B5%D0%BC%D0%BB%D1%8F-%D1%84%D0%BB%D0%B0%D0%B3%D0%B8-%D1%84%D0%BB%D0%B0%D0%B3-1313553/). Лицензия [Pixabay License](https://pixabay.com/ru/service/license/).
25. Фото "Nao" (файл node24.jpg), автор [Brett Davis](https://www.flickr.com/photos/brettdavis/), 2010, [ссылка на источник](https://www.flickr.com/photos/brettdavis/4436987052). [Лицензия Creative Commons Attribution-NonCommercial 2.0 Generic (CC BY-NC 2.0)](https://creativecommons.org/licenses/by-nc/2.0/).
26. Фото "70-я сессия Генеральной Ассамблеи ООН.", 2015, [ссылка на источник](http://kremlin.ru/events/president/news/50385/photos/41853), [все фотографии сайта Кремля распространяются](http://kremlin.ru/about/copyrights) по лицензии [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/).

Все изображения не перечисленные в данном списке созданы авторами этого репозитория (Дмитрием и Юлией Ращенко) в 2015 году и распространяются по лицензии [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/deed.ru), то есть при использовании требуют ссылки на этот репозиторий, ссылки на лицензию и указание того, вносились ли изменения.

All images not listed above was created by this repository authors (Dmitrii and Iuliia Rashchenko) in 2015 and is distributing with license [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/deed.ru), i.e. attribution (link to this repository), license link and changes indication required.

Программный код содержащаяся в данном репозитории разработан Дмитрием и Юлией Ращенко в 2015 году и распространяется по лицензии MIT (подробность в файле LICENSE). То есть допускается любое использование без ограничений при сохранении уведомления о лицензии. При этом ПО распространяется "Как есть" без каких либо гарантий со стороны авторов.

Program code in this repository developed by Dmitrii and Iuliia Rashchenko in 2015 and it is distributing with MIT license (check file LICENSE for further information).

В случае любых вопросов или претензий по поводу используемых в программе материалов, пожалуйста, обращайтесь по адресу dimitree54@gmail.com.

In case of any questions or claims about materials used feel free to contact with us by email dimitree54@gmail.com.